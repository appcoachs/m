package main

import (
	_ "bitbucket.org/appcoachs/m/config"
	"bitbucket.org/appcoachs/m/mdw"
	"fmt"
	"github.com/gorilla/context"
	"github.com/justinas/alice"
	"github.com/spf13/viper"
	"github.com/stretchr/graceful"
	//"net/http"
	"runtime"
	"time"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	mongodb := viper.GetStringMapString("mongodb")
	if scheme, ok := mongodb["scheme"]; ok {
		fmt.Printf("mongodb: %s\n", scheme)
	}
	host := viper.GetString("http")

	n := alice.New(
		mdw.LoggingHandler,
		mdw.TimeoutHandler,
		mdw.RecoverHandler,
		mdw.ThrottleHandler,
		context.ClearHandler,
	).Then(router())

	graceful.Run(host, 3*time.Second, n)
}
