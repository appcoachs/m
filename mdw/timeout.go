package mdw

import (
	"net/http"
	"time"
)

func TimeoutHandler(h http.Handler) http.Handler {
	return http.TimeoutHandler(h, 1*time.Second, "timed out")
}
