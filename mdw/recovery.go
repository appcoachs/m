package mdw

import (
	"log"
	"net/http"
	"runtime"
)

// NewRecovery returns a new instance of Recovery
func RecoverHandler(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				stack := make([]byte, 1024)
				stack = stack[:runtime.Stack(stack, false)]
				log.Printf("[PANIC] %s\n%s", err, stack)
			}
		}()
		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}
