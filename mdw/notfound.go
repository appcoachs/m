package mdw

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
)

type NotFound struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func NewNotFound() *NotFound {
	return &NotFound{
		Code:    404,
		Message: "not found",
	}
}

func (nf *NotFound) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	format := r.Header.Get("X-Response-Format")
	switch format {
	case "json":
		rw.Header().Set("Content-Type", "application/vnd.api+json; charset=utf-8")
		defer func() {
			json.NewEncoder(rw).Encode(nf)
		}()
	case "xml":
		rw.Header().Set("Content-Type", "application/xml")
		defer func() {
			fmt.Fprintf(rw, xml.Header)
			xml.NewEncoder(rw).Encode(nf)
		}()
	default:
		defer func() {
			fmt.Fprintf(rw, "404 Not Found")
		}()
	}

	rw.WriteHeader(http.StatusNotFound)
}
