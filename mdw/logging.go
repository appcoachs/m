package mdw

import (
	"github.com/gorilla/context"
	"github.com/gorilla/handlers"
	"net/http"
	//"net/http/httptest"
	"log"
	"os"
	"time"
)

func LoggingHandler(next http.Handler) http.Handler {
	f := func(w http.ResponseWriter, r *http.Request) {
		t := time.Now()
		context.Set(r, "time", t)
		next.ServeHTTP(w, r)
		log.Println("time estimate: ", time.Since(t))
	}

	return handlers.CombinedLoggingHandler(os.Stdout, http.HandlerFunc(f))
}
