package mdw

import (
	"github.com/PuerkitoBio/throttled"
	"github.com/PuerkitoBio/throttled/store"
	"net/http"
)

func ThrottleHandler(h http.Handler) http.Handler {
	/*th := throttled.Interval(
		throttled.PerSec(300),
		0,
		&throttled.VaryBy{Path: true},
		0,
	)*/
	th := throttled.RateLimit(
		throttled.PerMin(100000),
		&throttled.VaryBy{Path: true},
		store.NewMemStore(1000),
	)
	return th.Throttle(h)
}
