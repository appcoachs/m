all: config
	gin

run:
	go run *.go

bench:
	boom -n 20000 -c 300 http://localhost:3001/v1/getads

config:
	crypto set -plaintext /m/config/config.yml config.yml
