package api

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
	"net/http"
	//"time"
)

func Index(w http.ResponseWriter, r *http.Request) {
	msg := map[string]interface{}{
		"status":  200,
		"context": "/v1/showads",
	}

	p := context.Get(r, "params").(httprouter.Params)
	fmt.Println("params: %v", p)

	w.Header().Set("Content-Type", "application/vnd.api+json; charset=utf-8")
	json.NewEncoder(w).Encode(msg)
}
