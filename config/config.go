package config

import (
	"fmt"
	"github.com/spf13/viper"
)

func init() {
	viper.AddRemoteProvider("etcd", "http://127.0.0.1:4001", "/m/config/config.yml")
	viper.SetConfigType("yml") // because there is no file extension in a stream of bytes
	if err := viper.ReadRemoteConfig(); err != nil {
		fmt.Printf("read config from etcd err: %s\n", err)
		viper.SetConfigName("config")   // name of config file (without extension)
		viper.AddConfigPath("/etc/m/")  // path to look for the config file in
		viper.AddConfigPath("$HOME/.m") // call multiple times to add many search paths
		err := viper.ReadInConfig()     // Find and read the config file
		if err != nil {
			fmt.Printf("read config err: %s\n", err)
			setDefault()
		}
	}
}

func setDefault() {
	viper.SetDefault("mongodb", map[string]string{
		"scheme": "mongodb://localhost:27017/appcoachm",
	})
	viper.SetDefault("http", ":3001")
}
