package main

import (
	"bitbucket.org/appcoachs/m/api"
	"bitbucket.org/appcoachs/m/api/offline"
	"bitbucket.org/appcoachs/m/api/online"
	"bitbucket.org/appcoachs/m/mdw"
	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
	"net/http"
	//"github.com/spf13/viper"
)

func wrapHandler(next http.Handler) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		context.Set(r, "params", ps)
		next.ServeHTTP(w, r)
	}
}

func router() http.Handler {
	r := httprouter.New()
	r.GET("/", wrapHandler(http.HandlerFunc(api.Index)))
	r.GET("/v1/getads", wrapHandler(http.HandlerFunc(offline.GetAds)))
	r.POST("/v1/showads", wrapHandler(http.HandlerFunc(online.ShowAds)))
	r.NotFound = mdw.NewNotFound().ServeHTTP
	return r
}
